package stdc.attestationdeplacement;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import stdc.attestationdeplacement.Globals;
import stdc.attestationdeplacement.R;

public class ChoixMotif extends AppCompatActivity {

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.choixmotifs);
        Toolbar toolbar = findViewById(R.id.toolbar);

        toolbar.setTitle("Veuillez choisir le motif du déplacement :");
        setSupportActionBar(toolbar);


        TextView motif1 = findViewById(R.id.motif1);
        motif1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Globals.motif = getString(R.string.motif1);
                Intent intent = new Intent();
                setResult(RESULT_OK, intent);
                finish();
            }
        });
        TextView motif2 = findViewById(R.id.motif2);
        motif2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Globals.motif = getString(R.string.motif2);
                Intent intent = new Intent();
                setResult(RESULT_OK, intent);
                finish();
            }
        });
        TextView motif3 = findViewById(R.id.motif3);
        motif3.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Globals.motif = getString(R.string.motif3);
                Intent intent = new Intent();
                setResult(RESULT_OK, intent);
                finish();
            }
        });
        TextView motif4 = findViewById(R.id.motif4);
        motif4.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Globals.motif = getString(R.string.motif4);
                Intent intent = new Intent();
                setResult(RESULT_OK, intent);
                finish();
            }
        });
        TextView motif5 = findViewById(R.id.motif5);
        motif5.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Globals.motif = getString(R.string.motif5);
                Intent intent = new Intent();
                setResult(RESULT_OK, intent);
                finish();
            }
        });
        TextView motif6 = findViewById(R.id.motif6);
        motif6.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Globals.motif = getString(R.string.motif6);
                Intent intent = new Intent();
                setResult(RESULT_OK, intent);
                finish();
            }
        });
        TextView motif7 = findViewById(R.id.motif7);
        motif7.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Globals.motif = getString(R.string.motif7);
                Intent intent = new Intent();
                setResult(RESULT_OK, intent);
                finish();
            }
        });

    }
}

