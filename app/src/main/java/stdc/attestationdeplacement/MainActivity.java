package stdc.attestationdeplacement;

import android.Manifest;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import stdc.attestationdeplacement.BDD;
import stdc.attestationdeplacement.ChoixMotif;
import stdc.attestationdeplacement.Globals;
import stdc.attestationdeplacement.R;

import android.os.Environment;
import android.os.StrictMode;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.MimeTypeMap;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class MainActivity extends AppCompatActivity {

    private static final int VERSION_BDD = 1;
    private static final String NOM_BDD = "Attestation.db";

    private String NOM = "NOM";
    private String PRENOM = "PRENOM";
    private String DATENAISSANCE = "DATENAISSANCE";
    private String LIEUNAISSANCE = "LIEUNAISSANCE";
    private String ADRESSE = "ADRESSE";
    private String VILLE = "VILLE";
    private String CODEPOSTAL = "CODEPOSTAL";
    private String DATESORTIE = "DATESORTIE";
    private String HEURESORTIE = "HEURESORTIE";
    private String MOTIF = "MOTIF";


    Bitmap bitmap;
    String fname;
    final Calendar myCalendar = Calendar.getInstance();
    DatePickerDialog.OnDateSetListener datesortie;
    DatePickerDialog.OnDateSetListener datenaissance;
    TimePickerDialog.OnTimeSetListener heuresortie;
    TextInputEditText edittextdatesortie;
    TextInputEditText edittextdatenaissance;
    TextInputEditText edittextheuresortie;
    TextInputEditText edittextchoixmotif;
    TextInputEditText edittextchoixprenom;
    TextInputEditText edittextchoixnom;
    TextInputEditText edittextchoixlieuNaissance;
    TextInputEditText edittextchoixadresse;
    TextInputEditText edittextchoixville;
    TextInputEditText edittextchoixcodepostal;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (Globals.getBasedeDonnee() == null) {
            Globals.setBasedeDonnee(new BDD(this, NOM_BDD, null, VERSION_BDD));
            creationBDD();
        }


        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (edittextdatesortie.getText().toString().isEmpty() ||
                        edittextdatenaissance.getText().toString().isEmpty()  ||
                        edittextheuresortie.getText().toString().isEmpty()  ||
                        edittextchoixmotif.getText().toString().isEmpty()  ||
                        edittextchoixprenom.getText().toString().isEmpty() ||
                        edittextchoixnom.getText().toString().isEmpty()  ||
                        edittextchoixlieuNaissance.getText().toString().isEmpty()  ||
                        edittextchoixadresse.getText().toString().isEmpty()  ||
                        edittextchoixville.getText().toString().isEmpty() ||
                        edittextchoixcodepostal.getText().toString().isEmpty() ) {

                    AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create(); //Read Update
                    alertDialog.setTitle("Erreur lors de la génération");
                    alertDialog.setMessage("Veuillez renseigner tous les champs");

                    alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // here you can add functions
                        }
                    });

                    alertDialog.show();  //<-- See This!
                } else {

                    Globals.getBasedeDonnee().updateInfo(NOM, edittextchoixnom.getText().toString());
                    Globals.getBasedeDonnee().updateInfo(PRENOM, edittextchoixprenom.getText().toString());
                    Globals.getBasedeDonnee().updateInfo(DATENAISSANCE, edittextdatenaissance.getText().toString());
                    Globals.getBasedeDonnee().updateInfo(LIEUNAISSANCE, edittextchoixlieuNaissance.getText().toString());
                    Globals.getBasedeDonnee().updateInfo(ADRESSE, edittextchoixadresse.getText().toString());
                    Globals.getBasedeDonnee().updateInfo(VILLE, edittextchoixville.getText().toString());
                    Globals.getBasedeDonnee().updateInfo(CODEPOSTAL, edittextchoixcodepostal.getText().toString());
                    Globals.getBasedeDonnee().updateInfo(DATESORTIE, edittextdatesortie.getText().toString());
                    Globals.getBasedeDonnee().updateInfo(HEURESORTIE, edittextheuresortie.getText().toString());
                    Globals.getBasedeDonnee().updateInfo(MOTIF, edittextchoixmotif.getText().toString());

                    BitmapDrawable a = writeTextOnDrawable(R.drawable.attestation);

                    bitmap = a.getBitmap();

                    SimpleDateFormat dateFormat = new SimpleDateFormat("ddMMyyyy");
                    SimpleDateFormat heureFormat = new SimpleDateFormat("HHmm");
                    dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
                    Date today = Calendar.getInstance().getTime();
                    String DateHeureCreation = dateFormat.format(today) +  heureFormat.format(today).replace(":","h");


                    if (isWriteStoragePermissionGranted()) {
                        saveImage(bitmap, "Attestation" + DateHeureCreation );
                        if (isReadStoragePermissionGranted()) {
                            openImage();
                        }
                    }

                }
            }
        });



        edittextchoixnom= findViewById(R.id.Nom);
        edittextchoixprenom = findViewById(R.id.Prenom);
        edittextdatenaissance= findViewById(R.id.DateNaissance);
        edittextchoixlieuNaissance= findViewById(R.id.LieuNaissance);
        edittextchoixadresse= findViewById(R.id.Adresse);
        edittextchoixville= findViewById(R.id.Ville);
        edittextchoixcodepostal= findViewById(R.id.CodePostal);
        edittextdatesortie= findViewById(R.id.DateSortie);
        edittextheuresortie= findViewById(R.id.HeureSortie);
        edittextchoixmotif= findViewById(R.id.Motif);

        edittextchoixnom.setText(Globals.getBasedeDonnee().getInfo(NOM));
        edittextchoixprenom.setText(Globals.getBasedeDonnee().getInfo(PRENOM));
        edittextdatenaissance.setText(Globals.getBasedeDonnee().getInfo(DATENAISSANCE));
        edittextchoixlieuNaissance.setText(Globals.getBasedeDonnee().getInfo(LIEUNAISSANCE));
        edittextchoixadresse.setText(Globals.getBasedeDonnee().getInfo(ADRESSE));
        edittextchoixville.setText(Globals.getBasedeDonnee().getInfo(VILLE));
        edittextchoixcodepostal.setText(Globals.getBasedeDonnee().getInfo(CODEPOSTAL));
        edittextdatesortie.setText(Globals.getBasedeDonnee().getInfo(DATESORTIE));
        edittextheuresortie.setText(Globals.getBasedeDonnee().getInfo(HEURESORTIE));
        edittextchoixmotif.setText(Globals.getBasedeDonnee().getInfo(MOTIF));
        Globals.motif = Globals.getBasedeDonnee().getInfo(MOTIF);

        datesortie = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabelDateSortie();
            }

        };

        edittextdatesortie.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(MainActivity.this, datesortie, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        datenaissance = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabelDateNaissance();
            }

        };

        edittextdatenaissance.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(MainActivity.this, datenaissance, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });


        heuresortie = new TimePickerDialog.OnTimeSetListener() {

            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                myCalendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
                myCalendar.set(Calendar.MINUTE, minute);
                updateLabelHeureSortie();
            }

        };

        edittextheuresortie.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new TimePickerDialog(MainActivity.this, heuresortie, myCalendar
                        .get(Calendar.HOUR_OF_DAY), myCalendar.get(Calendar.MINUTE), true).show();
            }
        });

        edittextchoixmotif.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Intent myIntent = new Intent(v.getContext(), ChoixMotif.class);
                startActivityForResult(myIntent, 0);
            }
        });

    }

    private void updateLabelDateSortie() {
        String myFormat = "dd/MM/yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        edittextdatesortie.setText(sdf.format(myCalendar.getTime()));
    }
    private void updateLabelDateNaissance() {
        String myFormat = "dd/MM/yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        edittextdatenaissance.setText(sdf.format(myCalendar.getTime()));
    }
    private void updateLabelHeureSortie() {
        String myFormat = "HH:mm"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        edittextheuresortie.setText(sdf.format(myCalendar.getTime()).replace(":","h"));
    }


    private BitmapDrawable writeTextOnDrawable(int drawableId) {
        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), drawableId)
                .copy(Bitmap.Config.ARGB_8888, true);

        Canvas canvas = new Canvas(bitmap);

        float scale = getResources().getDisplayMetrics().density;
        Paint paintText = new Paint(Paint.ANTI_ALIAS_FLAG);
        paintText.setColor(Color.rgb(0, 0, 0));
        paintText.setTextSize((int) (15 * scale));
        paintText.setShadowLayer(1f, 0f, 1f, Color.WHITE);

        String NomPrenom = edittextchoixprenom.getText().toString() + " " + edittextchoixnom.getText().toString();
        Rect boundsPrenom = new Rect();
        paintText.getTextBounds(NomPrenom, 0, NomPrenom.length(), boundsPrenom);
        int NomPrenomx = 530;//(bitmap.getWidth() - bounds.width())/2;
        int NomPrenomy = 680;//(bitmap.getHeight() + bounds.height())/2;
        canvas.drawText(NomPrenom, NomPrenomx, NomPrenomy, paintText);


        String NeeLe = edittextdatenaissance.getText().toString();
        Rect boundsNeeLe = new Rect();
        paintText.getTextBounds(NeeLe, 0, NeeLe.length(), boundsNeeLe);
        int NeeLex = 530;//(bitmap.getWidth() - bounds.width())/2;
        int NeeLey = 780;//(bitmap.getHeight() + bounds.height())/2;
        canvas.drawText(NeeLe, NeeLex, NeeLey, paintText);

        String LieuNaissance = edittextchoixlieuNaissance.getText().toString();
        Rect boundsLieuNaissance = new Rect();
        paintText.getTextBounds(LieuNaissance, 0, LieuNaissance.length(), boundsLieuNaissance);
        int LieuNaissancex = 400;//(bitmap.getWidth() - bounds.width())/2;
        int LieuNaissancey = 887;//(bitmap.getHeight() + bounds.height())/2;
        canvas.drawText(LieuNaissance, LieuNaissancex, LieuNaissancey, paintText);


        String Adresse = edittextchoixadresse.getText().toString() + " " + edittextchoixcodepostal.getText().toString() + " " + edittextchoixville.getText().toString();
        Rect boundsAdresse = new Rect();
        paintText.getTextBounds(Adresse, 0, Adresse.length(), boundsAdresse);
        int Adressex = 580;//(bitmap.getWidth() - bounds.width())/2;
        int Adressey = 990;//(bitmap.getHeight() + bounds.height())/2;
        canvas.drawText(Adresse, Adressex, Adressey, paintText);

        String AdresseSignature = edittextchoixville.getText().toString();
        Rect boundsAdresseSignature = new Rect();
        paintText.getTextBounds(AdresseSignature, 0, AdresseSignature.length(), boundsAdresseSignature);
        int AdresseSignaturex = 480;//(bitmap.getWidth() - bounds.width())/2;
        int AdresseSignaturey = 2665;//(bitmap.getHeight() + bounds.height())/2;
        canvas.drawText(AdresseSignature, AdresseSignaturex, AdresseSignaturey, paintText);

        String DateSortie = edittextdatesortie.getText().toString() + " à " + edittextheuresortie.getText().toString();
        Rect boundsDateSortie = new Rect();
        paintText.getTextBounds(DateSortie, 0, DateSortie.length(), boundsDateSortie);
        int DateSortiex = 430;//(bitmap.getWidth() - bounds.width())/2;
        int DateSortiey = 2770;//(bitmap.getHeight() + bounds.height())/2;
        canvas.drawText(DateSortie, DateSortiex, DateSortiey, paintText);



        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        SimpleDateFormat heureFormat = new SimpleDateFormat("HH:mm");
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date today = Calendar.getInstance().getTime();

        Paint paintTextCreation = new Paint(Paint.ANTI_ALIAS_FLAG);
        paintTextCreation.setColor(Color.rgb(0, 0, 0));
        paintTextCreation.setTextSize((int) (10 * scale));
        paintTextCreation.setShadowLayer(1f, 0f, 1f, Color.WHITE);

        String DateHeureCreation = dateFormat.format(today) + " à " + heureFormat.format(today).replace(":","h");
        Rect boundsHeureCreation = new Rect();
        paintTextCreation.getTextBounds(DateHeureCreation , 0, DateHeureCreation.length(), boundsHeureCreation);
        int HeureCreationx = 1950;//(bitmap.getWidth() - bounds.width())/2;
        int HeureCreationy = 3025;//(bitmap.getHeight() + bounds.height())/2;
        canvas.drawText(DateHeureCreation, HeureCreationx, HeureCreationy, paintTextCreation);

        Paint paintTextMotif = new Paint(Paint.ANTI_ALIAS_FLAG);
        paintTextMotif.setColor(Color.rgb(0, 0, 0));
        paintTextMotif.setTextSize((int) (24 * scale));
        paintTextMotif.setShadowLayer(1f, 0f, 1f, Color.WHITE);
        Rect boundsMotif = new Rect();
        String Motif = "X";
        paintTextCreation.getTextBounds(Motif , 0, Motif.length(), boundsMotif);
        int Motifx = 325;//(bitmap.getWidth() - bounds.width())/2;
        int Motify = 0;
        String motif ="";
        if (Globals.motif.equals(getString(R.string.motif1))) {
            Motify = 1370;
            motif = "travail";
        }
        else if (Globals.motif.equals(getString(R.string.motif2))) {
            Motify = 1585;
            motif = "courses";
        }
        else if (Globals.motif.equals(getString(R.string.motif3))) {
            Motify = 1763;
            motif = "sante";
        }
        else if (Globals.motif.equals(getString(R.string.motif4))) {
            Motify = 1920;
            motif = "famille";
        }
        else if (Globals.motif.equals(getString(R.string.motif5))) {
            Motify = 2160;
            motif = "sport";
        }
        else if (Globals.motif.equals(getString(R.string.motif6))) {
            Motify = 2362;
            motif = "judiciaire";
        }
        else if (Globals.motif.equals(getString(R.string.motif7))) {
            Motify = 2520;
            motif = "missions";
        }

        canvas.drawText(Motif, Motifx, Motify, paintTextMotif);


        String QRCode = "Cree le: " + dateFormat.format(today) + " a " + heureFormat.format(today).replace(":","h") + ";" +
                " Nom: " + edittextchoixnom.getText().toString() + ";" +
                " Prenom:  " +  edittextchoixprenom.getText().toString() + ";" +
                " Naissance: " + edittextdatenaissance.getText().toString()  + " a " + edittextchoixlieuNaissance.getText().toString() + ";" +
                " Adresse: " + edittextchoixadresse.getText().toString() + " " + edittextchoixcodepostal.getText().toString() + " " + edittextchoixville.getText().toString() + ";" +
                " Sortie: " +  edittextdatesortie.getText().toString() + " a " + edittextheuresortie.getText().toString().replace(":","h") + ";" +
                " Motifs: " + motif;


        Bitmap bm = encodeAsBitmap(QRCode, 460, 460);
        canvas.drawBitmap(bm, 1800, 2510, null);

        Bitmap bm2 = encodeAsBitmap(QRCode, 1450, 1450);
        canvas.drawBitmap(bm2, 2700, 140, null);

        return new BitmapDrawable(getResources(), bitmap);
    }

    private void saveImage(Bitmap finalBitmap, String image_name) {

        String root = Environment.getExternalStorageDirectory().toString();
        File myDir = new File(root);
        myDir.mkdirs();
        fname = "Image-" + image_name+ ".jpg";
        File file = new File(myDir, fname);
        if (file.exists()) file.delete();
        Log.i("LOAD", root + fname);
        try {
            FileOutputStream out = new FileOutputStream(file);
            finalBitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void openImage()
    {
        File file = new File(Environment.getExternalStorageDirectory(),
                fname);

        Uri uri = FileProvider.getUriForFile(MainActivity.this, BuildConfig.APPLICATION_ID + ".provider",file);

        Intent intent = new Intent(android.content.Intent.ACTION_VIEW);
        String mime = "*/*";
        MimeTypeMap mimeTypeMap = MimeTypeMap.getSingleton();
        if (mimeTypeMap.hasExtension(
                MimeTypeMap.getFileExtensionFromUrl(uri.toString())))
            mime = mimeTypeMap.getMimeTypeFromExtension(
                    MimeTypeMap.getFileExtensionFromUrl(uri.toString()));
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        intent.setDataAndType(uri,mime);
        startActivity(intent);

    }


    public  boolean isReadStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                return true;
            } else {

                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 3);
                return false;
            }
        }
        else { //permission is automatically granted on sdk<23 upon installation
            return true;
        }
    }

    public  boolean isWriteStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                return true;
            } else {

                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 2);
                return false;
            }
        }
        else { //permission is automatically granted on sdk<23 upon installation
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 2:
                if(grantResults[0]== PackageManager.PERMISSION_GRANTED){
                    saveImage(bitmap, "Attestation");
                }else{
                }
                break;

            case 3:

                if(grantResults[0]== PackageManager.PERMISSION_GRANTED){
                    openImage();
                    //read //saveImage(bitmap, "Attestation");
                }else{
                }
                break;
        }
    }


    private void creationBDD() {
        Globals.getBasedeDonnee().insertInfo(NOM,"");
        Globals.getBasedeDonnee().insertInfo(PRENOM,"");
        Globals.getBasedeDonnee().insertInfo(DATENAISSANCE,"");
        Globals.getBasedeDonnee().insertInfo(LIEUNAISSANCE,"");
        Globals.getBasedeDonnee().insertInfo(ADRESSE,"");
        Globals.getBasedeDonnee().insertInfo(VILLE,"");
        Globals.getBasedeDonnee().insertInfo(CODEPOSTAL,"");
        Globals.getBasedeDonnee().insertInfo(DATESORTIE,"");
        Globals.getBasedeDonnee().insertInfo(HEURESORTIE,"");
        Globals.getBasedeDonnee().insertInfo(MOTIF,"");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (item.getItemId() == R.id.menu_info) {
            AlertDialog alertDialog = new AlertDialog.Builder(this).create();
            alertDialog.setTitle(R.string.app_name);

            TextView myMsg = new TextView(this);
            myMsg.setText(Html.fromHtml(getString(R.string.info)));

            myMsg.setGravity(Gravity.CENTER);
            myMsg.setTextSize(16);
            myMsg.setPadding(30, 50, 30, 0);
            ScrollView scrollPane = new ScrollView(this);
            scrollPane.addView(myMsg);
            alertDialog.setView(scrollPane);

            alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    // here you can add functions
                }
            });
            alertDialog.setIcon(R.drawable.ic_info_outline_white_48dp);
            alertDialog.show();
            myMsg.setMovementMethod(LinkMovementMethod.getInstance());
        }


        //noinspection SimplifiableIfStatement


        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (!Globals.motif.isEmpty()) {
                edittextchoixmotif.setText(Globals.motif);
            }
        }
    }

    public static Bitmap encodeAsBitmap(String source, int width, int height) {
        BitMatrix result;

        try {
            result = new MultiFormatWriter().encode(source, BarcodeFormat.QR_CODE, width, height, null);
        } catch (IllegalArgumentException | WriterException e) {
            // Unsupported format
            return null;
        }

        final int w = result.getWidth();
        final int h = result.getHeight();
        final int[] pixels = new int[w * h];

        for (int y = 0; y < h; y++) {
            final int offset = y * w;
            for (int x = 0; x < w; x++) {
                pixels[offset + x] = result.get(x, y) ? Color.BLACK : Color.WHITE;
            }
        }

        final Bitmap bitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        bitmap.setPixels(pixels, 0, width, 0, 0, w, h);

        return bitmap;
    }
}
