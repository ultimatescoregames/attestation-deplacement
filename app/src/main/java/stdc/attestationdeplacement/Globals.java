package stdc.attestationdeplacement;

import stdc.attestationdeplacement.BDD;

public class Globals {
    public static String motif;
    private static BDD BasedeDonnee;

    public static BDD getBasedeDonnee() {
        return BasedeDonnee;
    }

    public static void setBasedeDonnee(BDD basedeDonnee) {
        BasedeDonnee = basedeDonnee;
    }
}
