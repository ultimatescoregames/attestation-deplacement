package stdc.attestationdeplacement;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;


public class BDD extends SQLiteOpenHelper {

    private static final String CREATE_BDD_INFO = "CREATE TABLE " + "TABLE_INFO" + " ("
            + "COL_ID" + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + "COL_NOM_INFO" + " TEXT NOT NULL, "
            + "COL_VALUE_INFO" + " TEXT NOT NULL )";

    private SQLiteDatabase bdd;
    private Context myContext;

    public BDD(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
        myContext = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_BDD_INFO);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        int upgradeTo = oldVersion + 1;
        while (upgradeTo <= newVersion) {
            switch (upgradeTo) {
            }
            upgradeTo++;
        }
    }

    public long insertInfo(String nom_info_p, String valeur_p) {
        long retour = -1;
        open();
        String quJeux = "select COUNT(*) from "
                + "TABLE_INFO" + " WHERE "
                + "COL_NOM_INFO" + "=\"" + nom_info_p + "\"";
        Cursor curquJeux = bdd.rawQuery(quJeux, null);
        curquJeux.moveToFirst();
        if (curquJeux.getInt(0) == 0) {
            ContentValues values = new ContentValues();
            values.put("COL_NOM_INFO", nom_info_p);
            values.put("COL_VALUE_INFO", valeur_p);

            retour = bdd.insert("TABLE_INFO", null, values);
        }
        close();
        return retour;
    }

    public long updateInfo(String nom_info_p, String valeur_p) {

        long retour;
        ContentValues values = new ContentValues();
        open();
        values.put("COL_VALUE_INFO", valeur_p);

        String where = "COL_NOM_INFO" + "=?  ";
        String[] whereArgs = new String[1];
        whereArgs[0] = nom_info_p;

        retour = bdd.update("TABLE_INFO", values, where, whereArgs);
        close();

        return retour;
    }

    public String getInfo(String nom_info_p) {
        String valeur = "";
        open();
        ArrayList<String[]> array_list = new ArrayList<String[]>();

        String qu = "select " +
                "TABLE_INFO" + "." + "COL_VALUE_INFO" +
                " from " + "TABLE_INFO" + " WHERE " +
                "TABLE_INFO" + "." + "COL_NOM_INFO" + "=\"" + nom_info_p + "\";";
        Cursor cur = bdd.rawQuery(qu, null);

        while (cur.moveToNext()) {
            valeur = cur.getString(0);
        }
        if (cur != null && !cur.isClosed()) {
            cur.close();
        }
        close();
        return valeur;
    }

    public void open() {
        //on ouvre la BDD en écriture
        bdd = this.getWritableDatabase();
    }

    @Override
    public void close() {
        //on ferme l'accés à la BDD
        bdd.close();
    }

    public SQLiteDatabase getBDD() {
        return bdd;
    }
}
